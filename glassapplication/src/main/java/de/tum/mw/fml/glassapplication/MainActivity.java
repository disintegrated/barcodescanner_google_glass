package de.tum.mw.fml.glassapplication;

import com.google.android.glass.media.Sounds;
import com.google.android.glass.widget.CardBuilder;
import com.google.android.glass.widget.CardScrollAdapter;
import com.google.android.glass.widget.CardScrollView;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import com.google.android.glass.view.WindowUtils;

public class MainActivity extends Activity
{
    private final static String TAG = MainActivity.class.getSimpleName();
    private CardScrollView mCardScroller;
    private View mView;
    private String scannedResult;
    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        scannedResult = getString(R.string.empty_scannedResult);
        getWindow().requestFeature(WindowUtils.FEATURE_VOICE_COMMANDS); //to enable voice commands
        mView = buildView();

        mCardScroller = new CardScrollView(this);
        mCardScroller.setAdapter(new CardScrollAdapter() {
            @Override
            public int getCount() {
                return 1;
            }

            @Override
            public Object getItem(int position) {
                return mView;
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                return mView;
            }

            @Override
            public int getPosition(Object item) {
                if (mView.equals(item)) {
                    return 0;
                }
                return AdapterView.INVALID_POSITION;
            }
        });
        // Handle the TAP event.
        mCardScroller.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // Plays disallowed sound to indicate that TAP actions are not supported.
                AudioManager am = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
                am.playSoundEffect(Sounds.DISALLOWED);
            }
        });
        setContentView(mCardScroller);
    }
    @Override
    public boolean onCreatePanelMenu(int featureId, Menu menu)
    {
        if(featureId == WindowUtils.FEATURE_VOICE_COMMANDS)
        {
            getMenuInflater().inflate(R.menu.activity_menu, menu);
            return true;
        }
        return super.onCreatePanelMenu(featureId, menu);
    }
    @Override
    public boolean onMenuItemSelected(int featureId, MenuItem menuItem)
    {
        if(featureId == WindowUtils.FEATURE_VOICE_COMMANDS)
        {
            if(R.id.start_scan_menu_item == menuItem.getItemId())
            {
                Intent scanIntent = new Intent(getString(R.string.scan_action));
                Log.i(TAG, "starting activity CaptureActivity");
                startActivityForResult(scanIntent, Integer.parseInt(getString(R.string.scan_request_code)));
            }
            return true;
        }

        return super.onMenuItemSelected(featureId, menuItem);
    }
    @Override
    protected void onResume() {
        super.onResume();
        mCardScroller.activate();
    }

    @Override
    protected void onPause() {
        mCardScroller.deactivate();
        super.onPause();
    }

    /**
     * Builds a Glass styled "Hello World!" view using the {@link CardBuilder} class.
     */
    private View buildView() {
        CardBuilder card = new CardBuilder(this, CardBuilder.Layout.TEXT);

        card.setText(scannedResult);
        return card.getView();
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(requestCode == Integer.parseInt(getString(R.string.scan_request_code)))
        {
            if(RESULT_OK == resultCode)
            {
                Bundle extras = data.getExtras();
                scannedResult = extras.getString(getString(R.string.scan_result_key));
                double scanDuration = extras.getDouble(getString(R.string.scan_duration_key));

                scannedResult += ". Duration of scan : "+scanDuration+" sec";
                mView = buildView();
                mCardScroller.getAdapter().notifyDataSetChanged();
            }
            else
            {
                scannedResult = getString(R.string.scan_failed);
                mView = buildView();
                mCardScroller.getAdapter().notifyDataSetChanged();
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }
}
