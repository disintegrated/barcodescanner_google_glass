package de.tum.mw.fml.glassapplication;

import com.google.zxing.Result;

import android.app.Activity;
import android.content.Intent;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.util.Log;
import android.widget.TextView;

import com.google.zxing.client.glass.*;
import com.google.zxing.client.result.*;
import java.io.IOException;

public class CaptureActivity extends Activity implements SurfaceHolder.Callback, GestureDetector.OnGestureListener, Camera.OnZoomChangeListener
{
    private static final String TAG = CaptureActivity.class.getSimpleName();

    private Camera mCamera;
    private Result mResult;
    private SurfaceHolder mSurfaceHolderWithCallBack;
    private boolean hasSurface;
    private boolean returnResult;
    private DecodeRunnable decodeRunnable;

    private long startTime;
    private long endTime;

    private GestureDetector mGestureDetector;

    @Override
    protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        Log.i(TAG, "On Create");
        Intent intent = getIntent();
        returnResult = intent != null && getString(R.string.scan_action).equals(intent.getAction());
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.capture);
        mGestureDetector = new GestureDetector(this, this);
    }

    @Override
    public synchronized void onResume() {
        Log.i(TAG, "On Resume");
        super.onResume();
        SurfaceView surfaceView = (SurfaceView) findViewById(R.id.preview_view);
        SurfaceHolder surfaceHolder = surfaceView.getHolder();
        if(surfaceHolder == null)
        {
            throw new IllegalStateException("No Surface Holder");
        }
        if(hasSurface)
        {
            SafeCameraOpen(surfaceHolder);
        }
        else
        {
            surfaceHolder.addCallback(this);
            mSurfaceHolderWithCallBack = surfaceHolder;
        }
    }

    @Override
    protected void onPause()
    {
        mResult = null;
        if (decodeRunnable != null)
        {
            decodeRunnable.stop();
            decodeRunnable = null;
        }
        if(mCamera != null)
        {
            setCamera(null, null);
        }
        if(mSurfaceHolderWithCallBack != null)
        {
            mSurfaceHolderWithCallBack.removeCallback(this);
            mSurfaceHolderWithCallBack = null;
        }
        super.onPause();
    }

    @Override
    public synchronized void surfaceCreated(SurfaceHolder surfaceHolder) {
        Log.i(TAG, "surfaceCreated: ");
        mSurfaceHolderWithCallBack = null; //reassigned in onResume()
        if (!hasSurface)
        {
            hasSurface = true;
            SafeCameraOpen(surfaceHolder);
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2)
    {
        //do  nothing for now
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder)
    {
        Log.i(TAG, getString(R.string.surface_destroyed));
        setCamera(null, null);
        mSurfaceHolderWithCallBack = null;
        hasSurface = false;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (mResult != null) {
            switch (keyCode) {
                case KeyEvent.KEYCODE_DPAD_CENTER:
                    handleResult(mResult);
                    return true;
                case KeyEvent.KEYCODE_BACK:
                    reset();
                    return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    private void SafeCameraOpen(SurfaceHolder holder)
    {
        //record time from when camera starts
        startTime = System.currentTimeMillis();
        Log.i(TAG, "SafeCameraOpen: startTime : "+startTime);
        try
        {
            releaseCameraAndPreview();
            Camera camera = Camera.open();
            setCamera(camera, holder);
        }
        catch (Exception  e)
        {
            Log.e(TAG, getString(R.string.camera_open_failed));
            e.printStackTrace();
        }


        //start decoder in another thread
        decodeRunnable = new DecodeRunnable(this, mCamera);
        new Thread(decodeRunnable).start();
        reset();
    }

    private void releaseCameraAndPreview()
    {
        setCamera(null, null);
    }

    /*
    * source: http://developer.android.com/training/camera/cameradirect.html*/
    private void setCamera(Camera camera, SurfaceHolder holder)
    {
        if(mCamera == camera)
        {
            return;
        }

        stopPreviewAndFreeCamera();
        mCamera = camera;

        if(mCamera != null)
        {
            CameraConfigurationManager.configure(mCamera);

            try
            {
                mCamera.setPreviewDisplay(holder);
            }
            catch (IOException e)
            {
                Log.e(TAG, getString(R.string.cannot_start_preview));
            }
            mCamera.startPreview();
        }
    }

    /*
    * source : http://developer.android.com/training/camera/cameradirect.html*/
    private void stopPreviewAndFreeCamera()
    {
        if (mCamera != null) {
            // Call stopPreview() to stop updating the preview surface.
            mCamera.stopPreview();

            // Important: Call release() to release the camera for use by other
            // applications. Applications should release the camera immediately
            // during onPause() and re-open() it during onResume()).
            mCamera.release();

            mCamera = null;
        }
    }

    void setResult(Result result)
    {
        //record the end of decoding
        endTime = System.currentTimeMillis();
        Log.i(TAG, "setResult: endtime : "+endTime);
        //stop the decoder thread
        decodeRunnable.stop();
        if (returnResult)
        {
            Intent scanResult = new Intent(getString(R.string.scan_action));
            Bundle extras = new Bundle();
            double duration = (endTime - startTime)/1000.0d;
            if(null == result)
            {
                extras.putString(getString(R.string.scan_result_key), "Empty");
                Log.i(TAG, "setResult: scan result was null");
                setResult(RESULT_CANCELED);
            }
            else
            {
                extras.putString(getString(R.string.scan_result_key), result.getText());
                Log.i(TAG, "setResult: scan result was not null");
                setResult(RESULT_OK, scanResult);
            }

            extras.putDouble(getString(R.string.scan_duration_key), duration);
            scanResult.putExtras(extras);
            finish();
        } else {
            TextView statusView = (TextView) findViewById(R.id.status_view);
            if(null == result)
            {
                statusView.setText(getString(R.string.scan_failed));
                statusView.setVisibility(View.VISIBLE);
            }
            else
            {
                String text = result.getText();
                statusView.setText(text);
                statusView.setTextSize(TypedValue.COMPLEX_UNIT_SP, Math.max(14, 56 - text.length() / 4));
                statusView.setVisibility(View.VISIBLE);
                this.mResult = result;
            }
        }
    }

    private void handleResult(Result result) {
        ParsedResult parsed = ResultParser.parseResult(result);
        Intent intent;
        if (parsed.getType() == ParsedResultType.URI) {
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse(((URIParsedResult) parsed).getURI()));
        } else {
            intent = new Intent(Intent.ACTION_WEB_SEARCH);
            intent.putExtra("query", ((TextParsedResult) parsed).getText());
        }
        startActivity(intent);
    }

    private synchronized void reset() {
        TextView statusView = (TextView) findViewById(R.id.status_view);
        statusView.setVisibility(View.GONE);
        mResult = null;
        decodeRunnable.startScanning();
    }

    @Override
    public boolean onDown(MotionEvent e) {
        return false;
    }

    @Override
    public void onShowPress(MotionEvent e) {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        return false;
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        return false;
    }

    @Override
    public void onLongPress(MotionEvent e) {

    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        return false;
    }

    @Override
    public void onZoomChange(int zoomValue, boolean stopped, Camera camera) {

    }
}


/*public final class CaptureActivity extends Activity implements SurfaceHolder.Callback {

    private static final String TAG = CaptureActivity.class.getSimpleName();
    private static final String SCAN_ACTION = "de.tum.mw.fml.SCAN";

    private boolean hasSurface;
    private boolean returnResult;
    private SurfaceHolder holderWithCallback;
    private Camera camera;
    private DecodeRunnable decodeRunnable;
    private Result result;

    private long startTime;
    private long endTime;

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        Log.i(TAG, "onCreate: My Barcode scanner app CaptureActivity");
        // returnResult should be true if activity was started using
        // startActivityForResult() with SCAN_ACTION intent
        Intent intent = getIntent();
        returnResult = intent != null && SCAN_ACTION.equals(intent.getAction());

        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.capture);
    }

    @Override
    public synchronized void onResume() {
        super.onResume();
        SurfaceView surfaceView = (SurfaceView) findViewById(R.id.preview_view);
        SurfaceHolder surfaceHolder = surfaceView.getHolder();
        if (surfaceHolder == null) {
            throw new IllegalStateException("No SurfaceHolder?");
        }
        if (hasSurface) {
            initCamera(surfaceHolder);
        } else {
            surfaceHolder.addCallback(this);
            holderWithCallback = surfaceHolder;
        }
    }

    @Override
    public synchronized void onPause() {
        result = null;
        if (decodeRunnable != null) {
            decodeRunnable.stop();
            decodeRunnable = null;
        }
        if (camera != null) {
            camera.stopPreview();
            camera.release();
            camera = null;
        }
        if (holderWithCallback != null) {
            holderWithCallback.removeCallback(this);
            holderWithCallback = null;
        }
        super.onPause();
    }

    @Override
    public synchronized void surfaceCreated(SurfaceHolder holder) {
        Log.i(TAG, "Surface created");
        holderWithCallback = null;
        if (!hasSurface) {
            hasSurface = true;
            initCamera(holder);
        }
    }

    @Override
    public synchronized void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        // do nothing
    }

    @Override
    public synchronized void surfaceDestroyed(SurfaceHolder holder) {
        Log.i(TAG, "Surface destroyed");
        holderWithCallback = null;
        hasSurface = false;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (result != null) {
            switch (keyCode) {
                case KeyEvent.KEYCODE_DPAD_CENTER:
                    handleResult(result);
                    return true;
                case KeyEvent.KEYCODE_BACK:
                    reset();
                    return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    private void initCamera(SurfaceHolder holder) {

        startTime = System.currentTimeMillis();
        if (camera != null) {
            throw new IllegalStateException("Camera not null on initialization");
        }
        camera = Camera.open();
        if (camera == null) {
            throw new IllegalStateException("Camera is null");
        }

        CameraConfigurationManager.configure(camera);

        try {
            camera.setPreviewDisplay(holder);
            camera.startPreview();
        } catch (IOException e) {
            Log.e(TAG, "Cannot start preview", e);
        }

        decodeRunnable = new DecodeRunnable(this, camera);
        new Thread(decodeRunnable).start();
        reset();
    }

    void setResult(Result result) {
        endTime = System.currentTimeMillis();
        if (returnResult) {
            Intent scanResult = new Intent(getString(R.string.scan_action));
            Bundle extras = new Bundle();
            extras.putString(getString(R.string.scan_result_key), result.getText());
            long duration = (endTime - startTime)/1000;
            if(null == result)
            {
                Log.i(TAG, "setResult: scan result was null");
                setResult(RESULT_CANCELED);
            }
            else
            {
                Log.i(TAG, "setResult: scan result was not null");
                setResult(RESULT_OK, scanResult);
            }

            extras.putLong(getString(R.string.scan_duration_key), duration);
            scanResult.putExtras(extras);
            finish();
        } else {
            TextView statusView = (TextView) findViewById(R.id.status_view);
            String text = result.getText();
            statusView.setText(text);
            statusView.setTextSize(TypedValue.COMPLEX_UNIT_SP, Math.max(14, 56 - text.length() / 4));
            statusView.setVisibility(View.VISIBLE);
            this.result = result;
        }
    }

    private void handleResult(Result result) {
        ParsedResult parsed = ResultParser.parseResult(result);
        Intent intent;
        if (parsed.getType() == ParsedResultType.URI) {
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse(((URIParsedResult) parsed).getURI()));
        } else {
            intent = new Intent(Intent.ACTION_WEB_SEARCH);
            intent.putExtra("query", ((TextParsedResult) parsed).getText());
        }
        startActivity(intent);
    }

    private synchronized void reset() {
        TextView statusView = (TextView) findViewById(R.id.status_view);
        statusView.setVisibility(View.GONE);
        result = null;
        decodeRunnable.startScanning();
    }

}
*/