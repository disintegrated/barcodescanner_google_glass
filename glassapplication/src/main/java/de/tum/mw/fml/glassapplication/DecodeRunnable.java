package de.tum.mw.fml.glassapplication;

import com.google.zxing.client.glass.CameraConfigurationManager;
import com.google.zxing.*;
import com.google.zxing.common.HybridBinarizer;

import android.hardware.Camera;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import java.util.Arrays;
import java.util.EnumMap;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

/**
 * Created by RabbidDog on 11/10/2015.
 */
/*final class DecodeRunnable implements Runnable, Camera.PreviewCallback
{
    private static final String TAG = DecodeRunnable.class.getSimpleName();
    private final CaptureActivity activity;
    private final Camera camera;
    private Handler handler;
    private final CountDownLatch handlerInitLatch;
    private boolean running;
    private byte[] previewBuffer;
    private final int height;
    private final int width;

    DecodeRunnable(CaptureActivity captureActivity, Camera camera)
    {
        this.activity = captureActivity;
        this.camera = camera;
        running = true;
        handlerInitLatch = new CountDownLatch(1);
        Camera.Parameters parameters = camera.getParameters();
        Camera.Size previewSize = parameters.getPreviewSize();
        height = previewSize.height;
        width = previewSize.width;
        previewBuffer = new byte[(height * width *3)/2]; //getPreviewFormat() then use the getBitsPerPixel(int) / 8

    }

    private Handler getHandler()
    {
        try
        {
                handlerInitLatch.await();
        }
        catch(InterruptedException e)
        {
            //find what to do here
        }
        return handler;
    }

    private final class DecodeHandler extends Handler
    {
        private final Map<DecodeHintType,Object> hints;
        private int decodeAttemptCount;

        DecodeHandler()
        {
            hints = new EnumMap<>(DecodeHintType.class);
            hints.put(DecodeHintType.POSSIBLE_FORMATS, Arrays.asList(BarcodeFormat.AZTEC, BarcodeFormat.QR_CODE, BarcodeFormat.DATA_MATRIX));
            hints.put(DecodeHintType.TRY_HARDER, Boolean.TRUE);
            decodeAttemptCount = 0;
        }

        @Override
        public void handleMessage(Message message)
        {
            if (!running)
            {
                return;
            }
            switch (message.what)
            {
                case R.id.decode_start:
                    camera.setPreviewCallbackWithBuffer(DecodeRunnable.this);
                    camera.addCallbackBuffer(previewBuffer);
                    break;
                case R.id.decode:
                    decode((byte[]) message.obj);
                    break;
                case R.id.decode_succeeded:
                    final Result result = (Result) message.obj;
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            activity.setResult(result);
                        }
                    });
                    break;
                case R.id.decode_failed:
                    Arrays.fill(previewBuffer, (byte) 0x0);
                    if(++decodeAttemptCount < 500 )
                    {
                        Log.i(TAG, "Handle Message: decode failed. Attempt no. " + decodeAttemptCount);
                        camera.addCallbackBuffer(previewBuffer);
                    }
                    else
                    {
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                activity.setResult(null);
                            }
                        });
                    }
                    break;
                case R.id.quit:
                    running = false;
                    Looper.myLooper().quit();
                    break;

            }
        }
        private void decode(byte[] data) {
            Result rawResult = null;

            int subtendedWidth = width / CameraConfigurationManager.ZOOM;
            int subtendedHeight = height / CameraConfigurationManager.ZOOM;
            int excessWidth = width - subtendedWidth;
            int excessHeight = height - subtendedHeight;

            //long start = System.currentTimeMillis();
            PlanarYUVLuminanceSource source =
                    new PlanarYUVLuminanceSource(data,
                            width, height,
                            excessWidth / 2, excessHeight / 2,
                            subtendedWidth, subtendedHeight,
                            false);
            BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));
            try {
                rawResult = new MultiFormatReader().decode(bitmap, hints);
            } catch (ReaderException re) {
                Log.i(TAG, "decode exception" + re.getStackTrace());
            }

            //long end = System.currentTimeMillis();
            //Log.i(TAG, "Decode in " + (end - start));
            Handler handler = getHandler();
            if (rawResult == null) {
                handler.sendEmptyMessage(R.id.decode_failed); //obtainMessage more efficient than creating and allocating new message instances
            } else {
                Log.i(TAG, "Decode succeeded: " + rawResult.getText());
                handler.obtainMessage(R.id.decode_succeeded, rawResult).sendToTarget();
            };
        }

    }

    void startScanning()
    {
        getHandler().sendEmptyMessage(R.id.decode_start);
    }

    void stop()
    {
        getHandler().sendEmptyMessage(R.id.quit);
    }

    @Override
    public void onPreviewFrame(byte[] bytes, Camera camera)
    {
        if (running)
        {
            getHandler().obtainMessage(R.id.decode, bytes).sendToTarget();
        }
    }

    @Override
    public void run()
    {
        Looper.prepare();
        handler = new DecodeHandler();
        handlerInitLatch.countDown();
        Looper.loop();
    }
}*/

final class DecodeRunnable implements Runnable, Camera.PreviewCallback {

    private static final String TAG = DecodeRunnable.class.getSimpleName();

    private final CaptureActivity activity;
    private final Camera camera;
    private final int height;
    private final int width;
    private final byte[] previewBuffer;
    private boolean running;
    private Handler handler;
    private final CountDownLatch handlerInitLatch;

    DecodeRunnable(CaptureActivity activity, Camera camera) {
        this.activity = activity;
        this.camera = camera;
        Camera.Parameters parameters = camera.getParameters();
        Camera.Size previewSize = parameters.getPreviewSize();
        height = previewSize.height;
        width = previewSize.width;
        previewBuffer = new byte[(height * width * 3) / 2];
        running = true;
        handlerInitLatch = new CountDownLatch(1);
    }

    private Handler getHandler() {
        try {
            handlerInitLatch.await();
        } catch (InterruptedException ie) {
            // continue?
        }
        return handler;
    }


    @Override
    public void run() {
        Looper.prepare();
        handler = new DecodeHandler();
        handlerInitLatch.countDown();
        Looper.loop();
    }

    void startScanning() {
        getHandler().obtainMessage(R.id.decode_start).sendToTarget();
    }

    void stop() {
        getHandler().obtainMessage(R.id.quit).sendToTarget();
    }

    @Override
    public void onPreviewFrame(byte[] data, Camera camera) {
        if (running) {
            getHandler().obtainMessage(R.id.decode, data).sendToTarget();
        }
    }

    private final class DecodeHandler extends Handler {

        private final Map<DecodeHintType,Object> hints;
        private int decodeAttemptCount;

        DecodeHandler() {
            hints = new EnumMap<>(DecodeHintType.class);
            hints.put(DecodeHintType.POSSIBLE_FORMATS,
                    Arrays.asList(BarcodeFormat.DATA_MATRIX, BarcodeFormat.QR_CODE, BarcodeFormat.AZTEC, BarcodeFormat.CODE_39, BarcodeFormat.CODE_128));
        }

        @Override
        public void handleMessage(Message message) {
            if (!running) {
                return;
            }
            switch (message.what) {
                case R.id.decode_start:
                    camera.setPreviewCallbackWithBuffer(DecodeRunnable.this);
                    camera.addCallbackBuffer(previewBuffer);
                    break;
                case R.id.decode:
                    decode((byte[]) message.obj);
                    break;
                case R.id.decode_succeeded:
                    final Result result = (Result) message.obj;
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            activity.setResult(result);
                        }
                    });
                    break;
                case R.id.decode_failed:
                    Arrays.fill(previewBuffer, (byte) 0x0);
                    if(++decodeAttemptCount < 500 )
                    {
                        Log.i(TAG, "Handle Message: decode failed. Attempt no. " + decodeAttemptCount);
                        camera.addCallbackBuffer(previewBuffer);
                    }
                    else
                    {
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                activity.setResult(null);
                            }
                        });
                    }
                    break;
                case R.id.quit:
                    running = false;
                    Looper.myLooper().quit();
                    break;
            }
        }

        private void decode(byte[] data) {
            Result rawResult = null;

            int subtendedWidth = width / CameraConfigurationManager.ZOOM;
            int subtendedHeight = height / CameraConfigurationManager.ZOOM;
            int excessWidth = width - subtendedWidth;
            int excessHeight = height - subtendedHeight;

            //long start = System.currentTimeMillis();
            PlanarYUVLuminanceSource source =
                    new PlanarYUVLuminanceSource(data,
                            width, height,
                            excessWidth / 2, excessHeight / 2,
                            subtendedWidth, subtendedHeight,
                            false);
            BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));
            try {
                rawResult = new MultiFormatReader().decode(bitmap, hints);
            } catch (ReaderException re) {
                Log.i(TAG, "decode exception" + re.getStackTrace());
            }

            //long end = System.currentTimeMillis();
            //Log.i(TAG, "Decode in " + (end - start));
            Handler handler = getHandler();
            Message message;
            if (rawResult == null) {
                message = handler.obtainMessage(R.id.decode_failed);
            } else {
                Log.i(TAG, "Decode succeeded: " + rawResult.getText());
                message = handler.obtainMessage(R.id.decode_succeeded, rawResult);
            }
            message.sendToTarget();
        }

    }

}
